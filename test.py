import os
import random 

rand_string = os.urandom(255)
print(rand_string)


def randomBytes(n):
    return bytearray(random.getrandbits(8) for i in range(n))

print(randomBytes(255))
